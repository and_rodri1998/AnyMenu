package com.almr.androdri98.anymenu;

public class ModelLog {
    public String email;
    public String senha;

    public ModelLog() {
    }

    public ModelLog(String email, String senha) {
        this.email = email;
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
