package com.almr.androdri98.anymenu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RestoAdapter adapter;
    private List<RestoRview> listItems;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAuth = FirebaseAuth.getInstance();

//        Button btnPdf=(Button)findViewById(R.id.btnPdf);
//        btnPdf.setOnClickListener(new View.OnClickListener(){
//            @Override
//            public void onClick(View v){
//                Intent intent=new Intent(MainActivity.this, ScreenPdfActivity.class);
//                startActivity(intent);
//            }
//        });
//
        Button btnMapa=(Button)findViewById(R.id.btnMap);
        btnMapa.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent=new Intent(MainActivity.this, ScreenMapsActivity.class);
                startActivity(intent);
            }
        });


        recyclerView = (RecyclerView)findViewById(R.id.rvRestos);
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));

        listItems=new ArrayList<>();

//        for (int i=0; i<=20; i++){
//            RestoRview listItem=new RestoRview(
//                    "Bar&Restô",
//                    "Rua",
//                    "xxxx",
//                    "Bairro",
//                    "Fortaleza",
//                    "CE"
//            );
//
//            listItems.add(listItem);
//        }

        adapter=new RestoAdapter(listItems, MainActivity.this);

        recyclerView.setAdapter(adapter);


        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("baresrestos").addValueEventListener(
                new ValueEventListener(){
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        listItems.removeAll(listItems);
                        for (DataSnapshot snapshot:
                                dataSnapshot.getChildren()){
                            RestoRview resto= snapshot.getValue(RestoRview.class);

                            Log.v("E_VALUE","Data: " + resto.getNome());

                            listItems.add(resto);
                        }
                        adapter.notifyDataSetChanged();
//                        ProgressBar progressFrag =(ProgressBar)view.findViewById(R.id.progressList);
//                        progressFrag.setVisibility(view.GONE);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id=item.getItemId();
        FirebaseUser user = mAuth.getCurrentUser();
        if (id==R.id.actCad){
            if (user != null) {
                Intent intent= new Intent(MainActivity.this, DetalhesActivity.class);
                startActivity(intent);
            } else {
                Intent intent= new Intent(MainActivity.this, CadastroActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
            return true;
        }else if (id==R.id.actLog){
            if (user != null) {
                Intent intent= new Intent(MainActivity.this, DetalhesActivity.class);
                startActivity(intent);
            } else {
                Intent intent= new Intent(MainActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
