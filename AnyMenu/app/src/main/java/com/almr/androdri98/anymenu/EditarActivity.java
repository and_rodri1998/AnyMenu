package com.almr.androdri98.anymenu;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class EditarActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private static final String TAG="EditarActivity";
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar);

        mAuth = FirebaseAuth.getInstance();

        Button btnCancel=(Button)findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditarActivity.super.onBackPressed();
            }
        });
        Button btnSave=(Button)findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                EditText nome=findViewById(R.id.etBarResto);

                restoEdit restoEdit;

                if (nome.getText().length()==0){
                    Toast.makeText(EditarActivity.this,"Existem campos vazios",Toast.LENGTH_SHORT).show();
                }else{
                    restoEdit=new restoEdit(nome.getText().toString());
                    String uid=mAuth.getUid();
                    mDatabase = FirebaseDatabase.getInstance().getReference();
                    mDatabase.child("baresrestos").child(uid).child("nome").setValue(restoEdit.getNome());
                }

                EditarActivity.super.onBackPressed();
            }
        });
    }
}
