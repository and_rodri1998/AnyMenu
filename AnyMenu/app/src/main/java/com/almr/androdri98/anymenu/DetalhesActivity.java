package com.almr.androdri98.anymenu;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class DetalhesActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhes);

        mAuth = FirebaseAuth.getInstance();

        viewDetalhes(mAuth.getUid());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id=item.getItemId();
        if (id==R.id.itemEdit){
            Intent intent= new Intent(DetalhesActivity.this, EditarActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intent);
            return true;
        }else if(id==R.id.actLogout){
            FirebaseAuth.getInstance().signOut();
            Intent intent= new Intent(DetalhesActivity.this, MainActivity   .class);
            startActivity(intent);
            DetalhesActivity.this.finish();
        }

        return super.onOptionsItemSelected(item);
    }

    public void viewDetalhes(final String uid){
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabase.child("baresrestos").child(uid).addValueEventListener(
                new ValueEventListener(){
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                            Resto restoView= dataSnapshot.getValue(Resto.class);

                            Log.v("E_VALUE","Data: " + restoView.getNome());
                            TextView email=(TextView)findViewById(R.id.tvEmail);
                            TextView rua=(TextView)findViewById(R.id.tvRua);
                            TextView numero=(TextView)findViewById(R.id.tvNumero);
                            TextView bairro=(TextView)findViewById(R.id.tvBairro);

                            email.setText(restoView.getNome());
                            rua.setText(restoView.getRua());
                            numero.setText(restoView.getNumero());
                            bairro.setText(restoView.getBairro());

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                }
        );
    }
}
