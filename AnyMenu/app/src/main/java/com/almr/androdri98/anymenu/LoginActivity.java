package com.almr.androdri98.anymenu;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private static final String TAG="LoginActivity";
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mAuth = FirebaseAuth.getInstance();

        final ModelLog[] user = new ModelLog[1];

        Button btnEntrar=(Button)findViewById(R.id.btnEntrar);
        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText etEmail=findViewById(R.id.etEmail);
                EditText etSenha=findViewById(R.id.etSenha);
                if (etEmail.getText().length()==0||etSenha.getText().length()==0) {
                    Toast.makeText(LoginActivity.this,"Existem campos vazios",Toast.LENGTH_SHORT).show();
                }
                else{
                    String email = etEmail.getText().toString();
                    String senha = etSenha.getText().toString();
                    user[0] = new ModelLog(email, senha);
                    logar(user[0]);
                }
            }
        });
        Button btnCancel=(Button)findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.super.onBackPressed();
            }
        });
    }

    public void logar(final ModelLog resto){
        mAuth.signInWithEmailAndPassword(resto.getEmail(), resto.getSenha())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(LoginActivity.this, "Seja Bem vindo!",
                                    Toast.LENGTH_SHORT).show();

                            String uid=mAuth.getUid();

                            Intent intent=new Intent(LoginActivity.this,DetalhesActivity.class);
                            startActivity(intent);
//                            SignupActivity.this.finish();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
