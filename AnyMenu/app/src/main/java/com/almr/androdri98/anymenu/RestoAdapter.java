package com.almr.androdri98.anymenu;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class RestoAdapter extends RecyclerView.Adapter<RestoAdapter.ViewHolder> {

    private List<RestoRview> dados;
    private Context context;

    public RestoAdapter(List<RestoRview> dados, Context context) {
        this.dados = dados;
        this.context = context;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_resto, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final RestoRview itemBusca = dados.get(position);
        String endereco=itemBusca.getRua()+", "+itemBusca.getNumero()+", "+itemBusca.getBairro();

        holder.tvNomeResto.setText(itemBusca.getNome());
        holder.tvEnderecoResto.setText(endereco);
//        Picasso.with(context).load(itemBusca.getFtperfil()).resize(500,500).centerCrop().into(holder.imUser);

        holder.restoCick.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
//                Toast.makeText(context,"item clicado "+position, Toast.LENGTH_SHORT).show();
//
                Intent intent = new Intent(context, ScreenPdfActivity.class);
//
//                Bundle dadosPost=new Bundle();
//                dadosPost.putString("uriImgPerf",itemBusca.getFtperfil());
//                dadosPost.putString("descricao",itemBusca.getDescricao());
//                dadosPost.putString("nameUser",itemBusca.getNameuser());
//                dadosPost.putString("idAutPost",itemBusca.getKeyUser());
//                intent.putExtras(dadosPost);
//
                context.startActivity(intent);
//
//
            }
        });
    }

    @Override
    public int getItemCount() {
        return dados.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvNomeResto;
        TextView tvEnderecoResto;
        ConstraintLayout restoCick;
        public ViewHolder(View itemView){
            super(itemView);
            restoCick=itemView.findViewById(R.id.itemRestoClick);
            tvNomeResto =itemView.findViewById(R.id.tvNomeResto);
            tvEnderecoResto=itemView.findViewById(R.id.tvEndereco);
        }
    }
}
