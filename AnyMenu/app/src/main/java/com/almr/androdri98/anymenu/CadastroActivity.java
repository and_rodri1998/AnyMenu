package com.almr.androdri98.anymenu;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Random;

public class CadastroActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private static final String TAG="CadastroActivity";
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro);

        mAuth = FirebaseAuth.getInstance();
        final Resto[] resto = new Resto[1];
        final ModelLog[] restoLog = new ModelLog[1];

        Button btnCancel=(Button)findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CadastroActivity.super.onBackPressed();
            }
        });
        Button btnSave=(Button)findViewById(R.id.btnSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent=new Intent(CadastroActivity.this, DetalhesActivity.class);
//                startActivity(intent);
                EditText etEmail=findViewById(R.id.etEmail);
                EditText etSenha=findViewById(R.id.etSenha);
                EditText etSenhaConf=findViewById(R.id.etConfSenha);
                EditText etNome=findViewById(R.id.etBarResto);
                EditText etRua=findViewById(R.id.etRua);
                EditText etNumero=findViewById(R.id.etNumero);
                EditText etBairro=findViewById(R.id.etBairro);
                EditText etCidade=findViewById(R.id.etCidade);
                EditText etEstado=findViewById(R.id.etEstado);

                if (etEmail.getText().length()==0||etSenha.getText().length()==0||etSenhaConf.getText().length()==0||
                        etNome.getText().length()==0||etNome.getText().length()==0||etRua.getText().length()==0||
                        etNumero.getText().length()==0||etBairro.getText().length()==0||etCidade.getText().length()==0||
                        etEstado.getText().length()==0){
                    Toast.makeText(CadastroActivity.this,"Existem campos vazios", Toast.LENGTH_SHORT).show();
                }else{
                    String email=etEmail.getText().toString();
                    String senha=etSenha.getText().toString();
                    String senhaConf=etSenhaConf.getText().toString();
                    String nome=etNome.getText().toString();
                    String numero=etNumero.getText().toString();
                    String rua=etRua.getText().toString();
                    String bairro=etBairro.getText().toString();
                    String cidade=etCidade.getText().toString();
                    String estado=etEstado.getText().toString();
                    String arquivo="padrão";

                    if ((senha.equals(senhaConf))&&(etSenha.getText().length()>=6&&etSenhaConf.getText().length()>=6)){
                        resto[0] =new Resto(nome,email,rua,numero,bairro,cidade,estado,arquivo);
                        restoLog[0] =new ModelLog(email,senha);


                        criarConta(restoLog[0]);
                        logarAoCadastrar(resto[0], restoLog[0]);
                    }
                    else{
                        Toast.makeText(CadastroActivity.this, "Você possui uma senha " +
                                "" +
                                "com menos de 6 caracteres ou as senhas  não são iguais",Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }
    public void criarConta(final ModelLog resto){
        mAuth.createUserWithEmailAndPassword(resto.getEmail(), resto.getSenha())
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                // Sign in success, update UI with the signed-in user's information
                                Log.d(TAG, "createUserWithEmail:success");
                                Toast.makeText(CadastroActivity.this, "Cadastrado com sucesso.",
                                        Toast.LENGTH_SHORT).show();
//                                FirebaseUser user = mAuth.getCurrentUser();
                            } else {
                                // If sign in fails, display a message to the user.
                                Log.w(TAG, "createUserWithEmail:failure", task.getException());
                                Toast.makeText(CadastroActivity.this, "Authentication failed.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
    }
    public void logarAoCadastrar(final Resto resto, final ModelLog restoLogs){
        mAuth.signInWithEmailAndPassword(restoLogs.getEmail(), restoLogs.getSenha())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(CadastroActivity.this, "Seja Bem vindo!",
                                    Toast.LENGTH_SHORT).show();

                            String uid=mAuth.getUid();

                            mDatabase = FirebaseDatabase.getInstance().getReference();

                            mDatabase.child("baresrestos").child(uid).setValue(resto);

                            Intent intent=new Intent(CadastroActivity.this,DetalhesActivity.class);
                            startActivity(intent);
//                            SignupActivity.this.finish();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(CadastroActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
